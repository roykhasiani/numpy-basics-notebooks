#!/usr/bin/env python
# coding: utf-8

# #### Vertically Stacking Vectors/Matrices

# In[1]:


import numpy as np


# In[4]:


# vertical stacking:

v1 = np.array([1,2,3,4]) # 1D
v2 = np.array([5,6,7,8]) # 1D

np.vstack([v1,v2,v2,v1]) # returns a 2D vector


# In[8]:


# horizontal stacking

h1 = np.array([[1,2,3,4],[9,10,11,12]]) # 2D
h2 = np.array([[5,6,7,8],[13,14,15,16]]) # 2D

np.hstack([h1,h2]) # returns a 2D vector


# In[ ]:




