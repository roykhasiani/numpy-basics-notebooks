#!/usr/bin/env python
# coding: utf-8

# In[2]:


# to use numpy, make sure it's installed first,(pip install numpy).
#import it and assign a variable to the instance.
import numpy as np


# ## The Basics: Initialize an array in NumPy. Basic Math around it.

# In[7]:


# initialize an array.
# pass a list as an argument to an np.array method.
a = np.array([5,6,7])
print(a)


# In[12]:


b = np.array([[1,2,3], [3,4,5]])
print (b)


# In[20]:


c = np.array([[1,2,3], [3,4,5], [5,6,7]])
print (c)


# ## Dimensions

# In[21]:


print(a.ndim)
print(b.ndim)
print(c.ndim)


# In[22]:


print(a.shape)
print(b.shape)
print(c.shape)


# # Accessing Values in NumPy arrays

# In[23]:


c[:1]


# In[25]:


print(c[1,2])


# In[26]:


print(c[:,2])


# In[6]:


d = np.array([[1,2],[3,4],[5,6],[7,8]])
print (d)


# # 3-D Array (An array of two 2-D arrays)

# In[15]:


e = np.array([[[5,6],[7,8]],[[9,10],[11,12]]])


# In[16]:


e


# In[17]:


print (e)


# In[24]:


# Access values and mutate them
# Work from the oustide in
# Example, we wanna access 12: (sub-array, row, colum - indices start at 0

print(e[1,1,1]) # 12

print(e[0,1,0]) # 7

print (e[1,1,1] * e[0,1,0]) # multiply 12 by 7 - all other arithmetic operations work too!!


# In[27]:


# Mutate values
# Example, change second row of each sub-array:

e[:,1,:] = [[13,14], [15,16]] #output is: [[[ 5,6],[13,14]], [[9,10],[15,16]]]
print(e)


# ## Initializing pre-built types of arrays

# In[29]:


# Get a matrix of zeros with a specified shape
# Example, zeros matrix of vector 5
f = np.zeros(5)
print (f)


# In[33]:


# Get a zeros matrix of shape (2 by 3)
g = np.zeros((2,3))
print(g)


# In[34]:


g = np.zeros((3,3))
print(g)


# In[35]:


# Initializing ones matrix

g = np.ones((4,1,3))
print(g)


# In[39]:


# Initializing any other matrix with custom numbers
# Use np.full()
# full() takes two parameters - size and the custom number
# see below:

g = np.full((2,3), 32)
print(g)


# In[48]:


# Initialize a matrix of random DECIMAL numbers
# use random.rand()
# rand() DOES NOT take a tuple like full() or full_like() or ones() or zeros().
# pass in the shape directly as below:

g = np.random.rand(4,2)
print(g)


# In[54]:


# Initialize matrix of random WHOLE numbers
# usage is: numpy.random.randint(low, high=None, size=None, dtype='l')
# low is inclusive, high exclusive. For example, in the below demo, it will pick numbers from -4 to 4, excluding 5
# dtype='I' will not work with negative bounds, such as below:

g = np.random.randint(-4, 5, size=(2,3))
print(g)


# In[59]:


# IDENTITY MATRIX
# use np.identity(n)
# returns a square array of shape n*n; logically (n, n)
# defaults to float unless optional dtype is given

g = np.identity(4, dtype='I')
print(g)


# In[ ]:




