#!/usr/bin/env python
# coding: utf-8

# ## Mathematics

# In[1]:


import numpy as np


# #### Basic Arithmetic

# ##### Consists of element-wise computations

# In[2]:


a = np.array([2, 3, 4])


# In[3]:


a


# In[4]:


a + 2


# In[5]:


a * 2


# In[6]:


a / 2


# In[7]:


a % 2


# In[14]:


a ** 2


# In[9]:


b = np.array([11, 12, 13])


# In[10]:


a + b


# In[12]:


np.sin(a)


# In[13]:


np.cos(b)


# #### Linear Algebra

# ##### Mostly consists of matrix multiplications

# In[17]:


# Let's multiply two matrices
# Remember the columns of the first matrix have to be equal to the rows of the second matrix
# it will return a matrix of size (r1, c2)
# To multiply matrices, use the inbuilt method np.matmul(m1,m2)

m1 = np.ones((2,3))
print(m1)
m2 = np.full((3,2), 2)
print(m2)

np.matmul(m1,m2)


# In[20]:


# Determinant of matrix
# Sanity check, an identity matrix has a determinant of one:

c = np.identity(4)
np.linalg.det(c)


# In[21]:


# You can do more such as Inverse of a matrix,etc


# #### Statistics

# In[22]:


stats = np.array([[1,2,3],[4,5,6]])
stats


# In[23]:


# get minimum value
np.min(stats)


# In[25]:


# get maximum value
np.max(stats)


# In[29]:


# get min of axis (min of first row and min of second row)
np.min(stats, axis=1) # returns [1,4]


# In[30]:


# sum all elements in a matrix
np.sum(stats)


# In[ ]:




